<!DOCTYPE HTML>
<html lang="en-gb">
<head>
    <link rel="stylesheet" href="assets/colourPicker.css" />
    <script src="assets/colourPicker.js" defer></script>
</head>

<body>
<?php
isset($_REQUEST['colorPicker1']) ? $hHex1 = $_REQUEST['colorPicker1'] : $hHex1 = "#ff0000";
isset($_REQUEST['colorPicker2']) ? $hHex2 = $_REQUEST['colorPicker2'] : $hHex2 = "#0000ff";
isset($_REQUEST['colorPicker3']) ? $hHex3 = $_REQUEST['colorPicker3'] : $hHex3 = "#ffff00";
isset($_REQUEST['colorPicker4']) ? $hHex4 = $_REQUEST['colorPicker4'] : $hHex4 = "#0000ff";
$_REQUEST['numRows'] > 0 ? $rows = $_REQUEST['numRows'] : $rows = 100;
$_REQUEST['numCols'] > 0 ? $cols = $_REQUEST['numCols'] : $cols = 100;
?>

<form name="pickers" action="index.php" method="post">
    <div id="toolsToggler">
        <img src="assets/cog.png" alt="tools" />
    </div>

    <div id="tools">
        <div id="horizontal">
            <p>horizontal colour gradient</p>
            <input type="text" class="inp" id="colorPicker1" name="colorPicker1" 
                value="<?php echo $hHex1; ?>" />
            <div class="palette" id="colorPalette1"></div>

            <input type="text" class="inp" id="colorPicker2" name="colorPicker2" 
                value="<?php echo $hHex2; ?>" />
            <div class="palette" id="colorPalette2"></div>
        </div>

        <div id="vertical">
            <p>vertical colour gradient</p>
            <input type="text" class="inp" id="colorPicker3" name="colorPicker3" 
                value="<?php echo $hHex3; ?>" />
            <div class="palette" id="colorPalette3"></div>
            <br />

            <input type="text" class="inp" id="colorPicker4" name="colorPicker4" 
                value="<?php echo $hHex4; ?>" />
            <div class="palette" id="colorPalette4"></div>
        </div>

        <p>rows</p>
        <input type="text" id="numRows" name="numRows" class="inp"
            value="<?php echo $rows; ?>" />

        <p>cols</p>
        <input type="text" id="numCols" name="numCols" class="inp"
            value="<?php echo $cols; ?>" />

        <br />
        <input type="submit" value="go" class="button" />
    </div>

    <div id="spectrum">
    <?php include('spectrum.php'); ?>
    </div>

</form>

</body>
