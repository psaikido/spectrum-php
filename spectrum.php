<?php
//fprint_r($_REQUEST);
isset($_REQUEST['colorPicker1']) ? $hHex1 = $_REQUEST['colorPicker1'] : $hHex1 = "#ff0000";
isset($_REQUEST['colorPicker2']) ? $hHex2 = $_REQUEST['colorPicker2'] : $hHex2 = "#0000ff";
isset($_REQUEST['colorPicker3']) ? $hHex3 = $_REQUEST['colorPicker3'] : $hHex3 = "#ffff00";
isset($_REQUEST['colorPicker4']) ? $hHex4 = $_REQUEST['colorPicker4'] : $hHex4 = "#0000ff";
$_REQUEST['numRows'] > 0 ? $rows = $_REQUEST['numRows'] : $rows = 100;
$_REQUEST['numCols'] > 0 ? $cols = $_REQUEST['numCols'] : $cols = 100;

// Horizontal colour values
list($hRedStart, $hGreenStart, $hBlueStart) = sscanf($hHex1, "#%02x%02x%02x");
list($hRedEnd, $hGreenEnd, $hBlueEnd) = sscanf($hHex2, "#%02x%02x%02x");

$hRedStep = ($hRedEnd - $hRedStart) / ($cols - 1);
$hGreenStep = ($hGreenEnd - $hGreenStart) / ($cols - 1);
$hBlueStep = ($hBlueEnd - $hBlueStart) / ($cols - 1);
$hRedSteps = clrArray($hRedStart, $hRedEnd, $cols, $hRedStep);
$hGreenSteps = clrArray($hGreenStart, $hGreenEnd, $cols, $hGreenStep);
$hBlueSteps = clrArray($hBlueStart, $hBlueEnd, $cols, $hBlueStep);

// Vertical colour values
list($vRedStart, $vGreenStart, $vBlueStart) = sscanf($hHex3, "#%02x%02x%02x");
list($vRedEnd, $vGreenEnd, $vBlueEnd) = sscanf($hHex4, "#%02x%02x%02x");

$vRedStep = ($vRedEnd - $vRedStart) / ($cols - 1);
$vGreenStep = ($vGreenEnd - $vGreenStart) / ($cols - 1);
$vBlueStep = ($vBlueEnd - $vBlueStart) / ($cols - 1);
$vRedSteps = clrArray($vRedStart, $vRedEnd, $cols, $vRedStep);
$vGreenSteps = clrArray($vGreenStart, $vGreenEnd, $cols, $vGreenStep);
$vBlueSteps = clrArray($vBlueStart, $vBlueEnd, $cols, $vBlueStep);

function clrArray($start, $end, $cols, $step) {
    $clr = $start;

    for($c = 0, $maxCols = $cols; $c < $maxCols; $c++) {
        if ($c != 0) {
            $clr+= $step;
        }
        if ($c == $cols - 1) {
            $clr = $end;
        }

        $clrSteps[] = sprintf('%0.2f', $clr);
    }

    return $clrSteps;
}

function draw($red, $green, $blue, $isRowStart = false) {
    $sideSize = "5px";
    $border = "";

    $css = "height: $sideSize; 
            width: $sideSize; 
            border-top: $border;
            border-left: $border;
            float: left;
            background-color: rgb($red, $green, $blue);";

    if ($isRowStart) {
        $css.= "clear: left;";
    }

    echo "<div style='$css'>&nbsp;</div>";
}

function fprint_r($a) {
    echo '<pre>';
    print_r($a);
    echo '</pre>';
}

for($r = 0, $maxRows = $rows; $r < $maxRows; $r++) {
    for($c = 0, $maxCols = $cols; $c < $maxCols; $c++) {
        if ($c == 0) {
            draw($hRedSteps[$c], $hGreenSteps[$c], $hBlueSteps[$c], true);
        } else {
            draw($hRedSteps[$c], $hGreenSteps[$c], $hBlueSteps[$c], false);
        }

        draw($vRedSteps[$r], $vGreenSteps[$r], $vBlueSteps[$r], false);
    }
}

